
import React from "react";
import { Container, Row } from "reactstrap";
import DemoNavbar from "components/Navbars/DemoNavbar.js";
import Tabs from "./IndexSections/Tabs.js";
import SimpleFooter from "components/Footers/SimpleFooter.js";

class Index extends React.Component {
  componentDidMount() {
    document.documentElement.scrollTop = 0;
    document.scrollingElement.scrollTop = 0;
    this.refs.main.scrollTop = 0;
  }
  render() {
    return (
      <>
        <main ref="main">
          <DemoNavbar />
          <section className="section section-components">
            <Container>
              <Tabs />
            </Container>
          </section>
        </main>
        <SimpleFooter />
      </>
    );
  }
}

export default Index;
