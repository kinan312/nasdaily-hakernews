/* eslint-disable */
import React ,{memo, useEffect , useState} from "react";
import { connect } from 'react-redux';
import { getPostsById, getJobStories , clearMessages} from '../../actions'
import FlatList from 'flatlist-react';
import {slice} from 'lodash';
import Skeleton , { SkeletonTheme }from 'react-loading-skeleton';
import {
  Button,
  Spinner,
  Row,
  Alert
} from "reactstrap";
import Post from "./Post";

const JobsPostList = (props) => {
  const limit = 5;
  const [jobtartIndex , setjobtartIndex ] = useState(0);
  const [postsLenght , setPostsLenght ] = useState(5);
  const [jobPostsView , setjobPostsView] = useState([])
  const[postIds , setPostIds] = useState([]);
  const [visibleAlert, setVisibleAlert] = useState(false);

  const onDismiss = () => {
    setVisibleAlert(false);
    props.clearMessages();
  }

useEffect(() => {
    getjobList()
  }, [props.jobTapActive])

const  getjobList = () => {
    if(props.jobTapActive){
        props.getJobStories()
    } 
}
  
   useEffect(() => {
    getPostsByIds(postIds, 'job')
  }, [postIds])

    useEffect(() => {
    setjobPostsView( props.jobPost )
  }, [props.jobPost])

 

    useEffect(() => {
    setPostIds(slice(props.jobPosts, jobtartIndex , limit + jobtartIndex))
    setPostsLenght(props.jobPosts.length)
  }, [props.jobPosts])

  
  useEffect(() => {
     if (props.message) {
      setVisibleAlert(true)
    }
  }, [props.message]);


  const getPostsByIds = (postIds , type) => {
    if(postIds.length > 0){
      postIds.map( id => {
      props.getPostsById(id , type)
    })
    setjobtartIndex((limit + jobtartIndex + 1))
    }
  } 

  const loadMorejob = () => {
    setPostIds(slice(props.jobPosts, jobtartIndex , limit + jobtartIndex))
    setjobtartIndex((limit + jobtartIndex + 1))
  }


  const renderjobPosts = (item , index) => {
     return(
      <Post key={index}  data={item}/>
    )
  }

    return (
      <>   

                    <Alert color="danger" isOpen={visibleAlert} toggle={onDismiss}>
                      {props.message}
                    </Alert>
                    {jobPostsView.length > 0 ? (
                         <FlatList
                        list={jobPostsView}
                        renderItem={renderjobPosts}
                        renderWhenEmpty={() => <div>List is empty!</div>}
                      />

                    ) :
                        <SkeletonTheme color="#F2F2F2" highlightColor="#fff" >
                        <p>
                          <Skeleton count={5}   height={100}/>
                        </p>
                      </SkeletonTheme>
                    }
                   

                           {props.loading === true ? 
                          <Row className="justify-content-center">
                              <Spinner style={{ width: '3rem', height: '3rem' }} type="grow" color="#000" />
                          </Row>
                        
                        :
                        null
                        }
                      <Button className="load-more-btn" onClick={loadMorejob}> 
                        Load More
                      </Button>

                     
                      
               
      </>
    );
}


const mapStateToProps = ({ postR }) => {
    const {
        loading,
        jobPost,
        jobPosts,
        message
    } = postR;
    return {
        loading,
        jobPost,
        jobPosts,
        message
    };
};
export default connect(mapStateToProps, {
    getPostsById,
    getJobStories,
    clearMessages,
})(memo(JobsPostList));
