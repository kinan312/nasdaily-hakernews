/* eslint-disable */
import React ,{memo, useEffect , useState} from "react";
import { connect } from 'react-redux';
import { getPostsById, getShowStories , clearMessages} from '../../actions'
import FlatList from 'flatlist-react';
import {slice} from 'lodash';
import Skeleton , { SkeletonTheme }from 'react-loading-skeleton';
import {
  Button,
  Spinner,
  Row,
  Alert
} from "reactstrap";
import Post from "./Post";

const ShowPostList = (props) => {
  const limit = 5;
  const [showtartIndex , setshowtartIndex ] = useState(0);
  const [postsLenght , setPostsLenght ] = useState(5);
  const [showPostsView , setshowPostsView] = useState([])
  const[postIds , setPostIds] = useState([]);
  const [visibleAlert, setVisibleAlert] = useState(false);

  const onDismiss = () => {
    setVisibleAlert(false);
    props.clearMessages();
  }

useEffect(() => {
    getshowList()
  }, [props.showTapActive])

const  getshowList = () => {
    if(props.showTapActive){
        props.getShowStories()
    } 
}
  
   useEffect(() => {
    getPostsByIds(postIds, 'show')
  }, [postIds])

    useEffect(() => {
    setshowPostsView( props.showPost )
  }, [props.showPost])

 

    useEffect(() => {
    setPostIds(slice(props.showPosts, showtartIndex , limit + showtartIndex))
    setPostsLenght(props.showPosts.length)
  }, [props.showPosts])

  
  useEffect(() => {
     if (props.message) {
      setVisibleAlert(true)
    }
  }, [props.message]);


  const getPostsByIds = (postIds , type) => {
    if(postIds.length > 0){
      postIds.map( id => {
      props.getPostsById(id , type)
    })
    setshowtartIndex((limit + showtartIndex + 1))
    }
  } 

  const loadMoreshow = () => {
    setPostIds(slice(props.showPosts, showtartIndex , limit + showtartIndex))
    setshowtartIndex((limit + showtartIndex + 1))
  }


  const rendershowPosts = (item , index) => {
     return(
      <Post key={index}  data={item}/>
    )
  }

    return (
      <>   

                    <Alert color="danger" isOpen={visibleAlert} toggle={onDismiss}>
                      {props.message}
                    </Alert>
                    {showPostsView.length > 0 ? (
                         <FlatList
                        list={showPostsView}
                        renderItem={rendershowPosts}
                        renderWhenEmpty={() => <div>List is empty!</div>}
                      />

                    ) :
                        <SkeletonTheme color="#F2F2F2" highlightColor="#fff" >
                        <p>
                          <Skeleton count={5}   height={100}/>
                        </p>
                      </SkeletonTheme>
                    }
                   

                           {props.loading === true ? 
                          <Row className="justify-content-center">
                              <Spinner style={{ width: '3rem', height: '3rem' }} type="grow" color="#000" />
                          </Row>
                        
                        :
                        null
                        }
                      <Button className="load-more-btn" onClick={loadMoreshow}> 
                        Load More
                      </Button>

                     
                      
               
      </>
    );
}


const mapStateToProps = ({ postR }) => {
    const {
        loading,
        showPost,
        showPosts,
        message
    } = postR;
    return {
        loading,
        showPost,
        showPosts,
        message
    };
};
export default connect(mapStateToProps, {
    getPostsById,
    getShowStories,
    clearMessages,
})(memo(ShowPostList));
