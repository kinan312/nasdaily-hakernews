/* eslint-disable */
import React ,{memo, useEffect , useState} from "react";
import classnames from "classnames";
import { connect } from 'react-redux';
import * as moment from 'moment'
import {truncate} from 'lodash';
import {
  Card,
  CardBody,
  Row,
  Col
} from "reactstrap";
import CardLink from "reactstrap/lib/CardLink";

 const Post = (props) => {
    const [dateNow , setDate] = useState( moment())
    const [data , setData] = useState(props.data)

    const [postTime , setPostTime] = useState({
      hour: 0,
      minutes: 0,
      seconds: 0
    })
    useEffect(() => {
        setData(props.data)
        getDate()
  }, [props])

const getDate = () => {
    var postCreationDate = new moment.unix(data.time).format('YYYY/MM/DD HH:mm:ss')
    const time = {
      days: dateNow.diff(postCreationDate , 'days'),
      hour: dateNow.diff(postCreationDate , 'hours'),
      minutes: dateNow.diff(postCreationDate , 'minutes'),
      seconds: dateNow.diff(postCreationDate , 'seconds')
    }
    setPostTime(time)

  }

    return (
      <>
        <Row className="justify-content-center">
          <Col lg="12">
            <Card className="postCard" >
              <CardLink href={data.url}>
                  <p className="postTitle">{truncate(data.title, {
                        'length': 40,
                        'separator': ' '
                    })}</p>
                <CardBody className="postBody">
                    {truncate(data.text, {
                        'length': 100,
                        'separator': ' '
                    })}
                    <Row className="info-row">
                        <i class="fa fa-clock-o" style={{padding: '0px 5px 0px 0px '}} aria-hidden="true"></i>  
                        { '  ' + ((postTime.days > 0  ) ? postTime.days :  (postTime.hour > 0 ? postTime.hour : postTime.minutes)) +  '  '  +  (postTime.days > 0 ?  ' day ' :(postTime.hour > 0 ?  ' hour ' : 'minutes' )) +  '  ' + 'ago'}                         
                        {(data.type === 'job' ? null : '  |  ' + data.descendants + '  ' +  'comments')}
                    </Row>
                </CardBody>
              </CardLink>
                
               
            </Card>
          </Col>
          
        </Row>
      </>
    );
}

export default Post;


