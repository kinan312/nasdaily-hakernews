/* eslint-disable */
import React ,{memo, useEffect, useState} from "react";
import classnames from "classnames";
import { connect } from 'react-redux';
import {getNewPosts , newPostType, topPostType, bestPostType} from '../../actions'
import {CircleArrow as ScrollUpButton} from "react-scroll-up-button";
import { ScrollView } from "@cantonjs/react-scroll-view";
import {
  NavItem,
  NavLink,
  Nav,
  TabContent,
  TabPane,
  Row,
  Col
} from "reactstrap";
import BestPostList from "./BestPostList";
import TopPostList from "./TopPostList";
import NewPostList from "./NewPostList";
import AsksPostList from "./AsksPostList";
import JobsPostList from "./JobsPostList";
import ShowPostList from "./ShowPostList";



const TabsSection = (props) => {
const [newTapActive ,setNewTapActive ] = useState(true)
const [topTapActive ,setTopTapActive ] = useState(false)
const [bestTapActive ,setBestTapActive ] = useState(false)

const [askTapActive ,setAskTapActive ] = useState(false)
const [jobTapActive ,setJobTapActive ] = useState(false)
const [showTapActive ,setShowTapActive ] = useState(false)




  const setView = (posttypeId) => {
    switch (posttypeId) {
      case 1:
        setNewTapActive(true)
        setTopTapActive(false)
        setBestTapActive(false)

        setAskTapActive(false)
        setShowTapActive(false)
        setJobTapActive(false)
        setIconTabs(1);
        break;

      case 2:
         setNewTapActive(false)
        setTopTapActive(true)
        setBestTapActive(false)
         setAskTapActive(false)
        setShowTapActive(false)
        setJobTapActive(false)
        setIconTabs(2);
        break;

     case 3:
        setNewTapActive(false)
        setTopTapActive(false)
        setBestTapActive(true)
        setAskTapActive(false)
        setShowTapActive(false)
        setJobTapActive(false)
        setIconTabs(3);
        break;

     case 4:
        setNewTapActive(false)
        setTopTapActive(false)
        setBestTapActive(false)
        setAskTapActive(true)
        setShowTapActive(false)
        setJobTapActive(false)
        setIconTabs(4);
        break;

     case 5:
        setNewTapActive(false)
        setTopTapActive(false)
        setBestTapActive(false)
        setAskTapActive(false)
        setShowTapActive(false)
        setJobTapActive(true)
        setIconTabs(5);
        break;

     case 6:
        setNewTapActive(false)
        setTopTapActive(false)
        setBestTapActive(false)
        setAskTapActive(false)
        setShowTapActive(true)
        setJobTapActive(false)
        setIconTabs(6);
        break;
    }
   
  }


  const [iconTabs , setIconTabs] = React.useState(1) ;
    return (
      <>
        <Row className="justify-content-center">
          <Col lg="12">
            <div className="nav-wrapper">
              
              <Nav
                id="tabs-icons-text"
                pills
                role="tablist"
              >
              <ScrollView isHorizontal={true} className="custom-nav-scroll">
                <NavItem>
                  <NavLink
                    aria-selected={iconTabs === 1}
                    className={classnames(" mb-md-0", {
                      active: iconTabs === 1
                    })}
                    onClick={e =>  setView(1)  }
                    href="#pablo"
                    role="tab"
                  >
                    New
                  </NavLink>
                </NavItem>

                 <NavItem>
                  <NavLink
                    aria-selected={iconTabs === 2}
                    className={classnames(" mb-md-0", {
                      active: iconTabs === 2
                    })}
                    onClick={e => setView(2)}
                    href="#pablo"
                    role="tab"
                  >
                    Top
                  </NavLink>
                </NavItem>
                
                <NavItem>
                  <NavLink
                    aria-selected={iconTabs === 3}
                    className={classnames(" mb-md-0", {
                      active: iconTabs === 3
                    })}
                    onClick={e => setView(3)}
                    href="#pablo"
                    role="tab"
                  >
                    Best
                  </NavLink>
                </NavItem>

                 <NavItem>
                   <NavLink
                    aria-selected={iconTabs === 4}
                    className={classnames(" mb-md-0", {
                      active: iconTabs === 4
                    })}
                    onClick={e => setView(4)}
                    href="#pablo"
                    role="tab"
                  >
                    Asks
                  </NavLink>
                </NavItem>
               
                
                 <NavItem>
                  <NavLink
                    aria-selected={iconTabs === 5}
                    className={classnames(" mb-md-0", {
                      active: iconTabs === 5
                    })}
                    onClick={e => setView(5)}
                    href="#pablo"
                    role="tab"
                  >
                    Jobs
                  </NavLink>
                </NavItem>

                  

                  <NavItem>
                  <NavLink
                    aria-selected={iconTabs === 6}
                    className={classnames(" mb-md-0", {
                      active: iconTabs === 6
                    })}
                    onClick={e => setView(6)}
                    href="#pablo"
                    role="tab"
                  >
                    Show
                  </NavLink>
                  </NavItem>
                 </ScrollView>
              </Nav>
               
            </div>
            
                <TabContent activeTab={"iconTabs" + iconTabs}>
                  <TabPane tabId="iconTabs1">
                    <NewPostList newTapActive={newTapActive}/>
                  </TabPane>
                  <TabPane tabId="iconTabs2">
                   <TopPostList topTapActive={topTapActive}/>
                  </TabPane>
                  <TabPane tabId="iconTabs3">
                    <BestPostList bestTapActive={bestTapActive}/>
                  </TabPane>

                  <TabPane tabId="iconTabs4">
                    <AsksPostList askTapActive={askTapActive}/>
                  </TabPane>
                  <TabPane tabId="iconTabs5">
                    <JobsPostList jobTapActive={jobTapActive}/>
                  </TabPane>
                  <TabPane tabId="iconTabs6">
                    <ShowPostList showTapActive={showTapActive}/>
                  </TabPane>
                 
                </TabContent>
               
          </Col>
          <ScrollUpButton
            StopPosition={0}
            ShowAtPosition={150}
            EasingType='easeOutCubic'
            AnimationDuration={1000}
            style={{
              backgroundColor: '#fff',
              zIndex: 150,
              color: '#FFC700',
              height: '40px',
              bottom: '30px',
              width: '40px',
              padding: '3px 5px 3px 5px',
              border: '3px solid #7a7a7a'
            }}
           
          />
        </Row>

        
      </>
    );
}


const mapStateToProps = ({ postR }) => {
    const {
        loading,
    } = postR;
    return {
        loading,
    };
};
export default connect(mapStateToProps, {
    getNewPosts,
    newPostType,
    topPostType,
    bestPostType
})(memo(TabsSection));
