//AUTH
export const CLEAR_MESSAGE = 'clear_message';
export const GET_POSTS = 'get_posts';
export const GET_POSTS_FAIL = 'get_posts_fail';
export const GET_POSTS_SUCCESS = 'get_posts_success';
export const DONE_POST = 'done_post';

export const NEW_POST_TYPE = 'new_post_type';
export const TOP_POST_TYPE = 'top_post_type';
export const BEST_POST_TYPE = 'best_post_type';

export const GET_POST_BY_ID = 'get_post_by_id';
export const GET_POST_BY_ID_FAIL = 'get_post_by_id_fail';
export const GET_NEW_POST_BY_ID_SUCCESS = 'get_new_post_by_id_success';
export const GET_BEST_POST_BY_ID_SUCCESS = 'get_best_post_by_id_success';
export const GET_TOP_POST_BY_ID_SUCCESS = 'get_top_post_by_id_success';

export const GET_SHOW_POST_BY_ID_SUCCESS = 'get_show_post_by_id_success';
export const GET_JOB_POST_BY_ID_SUCCESS = 'get_job_post_by_id_success';
export const GET_ASK_POST_BY_ID_SUCCESS = 'get_ask_post_by_id_success';

export const GET_BEST_POSTS = 'get_best_post';
export const GET_BEST_POSTS_FAIL = 'get_best_post_fail';
export const GET_BEST_POSTS_SUCCESS = 'get_best_post_success';

export const GET_TOP_POSTS = 'get_top_post';
export const GET_TOP_POSTS_FAIL = 'get_top_post_fail';
export const GET_TOP_POSTS_SUCCESS = 'get_top_post_success';

export const GET_ASKS = 'get_asks';
export const GET_ASK_FAIL = 'get_ask_fail';
export const GET_ASK_SUCCESS = 'get_ask_success';


export const GET_JOBS = 'get_jobs';
export const GET_JOBS_FAIL = 'get_jobs_fail';
export const GET_JOBS_SUCCESS = 'get_jobs_success';

export const GET_SHOW = 'get_show';
export const GET_SHOW_FAIL = 'get_show_fail';
export const GET_SHOW_SUCCESS = 'get_show_success';


